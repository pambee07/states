import { useState } from "react";
import "./App.css";
import Title from "./Title";



function App() {
  const [count, setCount] = useState(0);

  function add() {
    setCount(count + 1);
  }

  function sub() {
    if (count > 0) {
      setCount(count - 1);
    }
  }

  function reset() {
    setCount(0);
  }
  return (
    <>
    <Title texte="Mon titre"/>
      <div>count is {count}</div>
      <button onClick={add}>add</button>
      <button onClick={sub}>sub</button>
      <button onClick={reset}>reset</button>
    </>
  );
}

export default App;
